package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    public String run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);

            String humanchoice = gethumanchoice();

            String computerchoice = getcomputerchoice();

            /**
             * Reads input from console with given prompt
             * @param prompt
             * @return string input answer from user
             */
            if (getwinner(humanchoice, computerchoice)) {
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". Computer win!");
                computerScore++;
            } else if ((getwinner(humanchoice, computerchoice)).equals(false)) {
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". Human win!");
                humanScore++;
            } else {
                System.out.println("Human chose " + humanchoice + ", computer chose " + computerchoice + ". It's a tie!"
                );
            }
            System.out.println("Score: human "+ humanScore+ ", computer " + computerScore);

            String cont = readInput("Do you wish to continue playing? (y/n)?");
            if (("y").equals(cont)) {
                roundCounter++;
                continue;
            }
            else if (!("y").equals(cont)) {
                System.out.println("Bye bye :)");
                return humanchoice;
            }
        }
    }
    // random choice for human and verifying the choice
        public String gethumanchoice() {
            while (true) {
                String choice = readInput("Your choice (Rock/Paper/Scissors)?");
                if (rpsChoices.contains(choice)) {
                    return choice;
                } else {
                    System.out.println("I do not understand " + choice + ". Could you try again?");
                }
            }
        }
        // random choice for computer
        public String getcomputerchoice () {   // to find a random choice for the computer
            String[] choices = {"rock", "paper", "scissors"};
            Double randNum = Math.random() * 3;
            int randInt = randNum.intValue();
            return choices[randInt];
        }

        public String readInput (String prompt){
            System.out.println(prompt);
            String userInput = sc.next();
            return userInput;
        }

        public Boolean getwinner(String winner, String loser) {
            if( (winner.equals("rock") && loser.equals("scissors")) ||
            (winner.equals("scissors") && loser.equals("paper")) ||
                    ( winner.equals("paper") && loser.equals("rock")))
                return true;
            else {
                return false;
            }
        }
}

